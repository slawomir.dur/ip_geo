#pragma once

#include <string>

namespace sofomo
{

struct Geolocation
{
    std::string ip;
    double latitude;
    double longitude;
};

}