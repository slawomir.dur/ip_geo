#include "tools/filesystem.hpp"

#include <filesystem>
#include <fstream>

namespace sofomo
{

namespace tools
{

File::File(const std::string& path) : path_{path} {}

bool File::exists() const
{
    return std::filesystem::exists(path_);
}

bool File::create() const
{
    std::fstream file{};
    file.open(path_, std::ios::app);
    if (!file.is_open()) return false;
    return exists();
}

bool File::remove() const
{
    return std::filesystem::remove(path_);
}

std::string File::path() const
{
    return path_;
}


}

}