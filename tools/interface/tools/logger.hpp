#pragma once

#include <string_view>
#include <sstream>
#include <memory>
#include <map>
#include <iostream>

#include "tools/format.hpp"

namespace sofomo
{
constexpr std::string_view info_prefix() { return "INFO"; }
constexpr std::string_view warning_prefix() { return "WARNING"; }
constexpr std::string_view error_prefix() { return "ERROR"; }

struct Logger
{
    using unique_ptr = std::unique_ptr<Logger>;

    Logger() = default;
    Logger(Logger&&) = default;
    Logger& operator=(Logger&&) = default;

    virtual ~Logger() {}
    virtual std::ostream& destination() = 0;
};

struct ConsoleLogger : public Logger
{
    ConsoleLogger() = default;
    ConsoleLogger(ConsoleLogger&&) = default;
    ConsoleLogger& operator=(ConsoleLogger&&) = default;

    std::ostream& destination() override { return std::cout; }
};

extern std::map<const char*, Logger::unique_ptr> loggers;

void register_logger(const char* name, Logger::unique_ptr&& logger);

std::size_t loggers_count();

void deregister_logger(const char* name);

void push_prefixed(std::string_view prefix, std::string_view module, std::string_view msg);

void log_info(std::string_view module, std::string_view msg);

void log_error(std::string_view module, std::string_view msg);

void log_warning(std::string_view module, std::string_view msg);

template <typename... Args>
void log_info(std::string_view module, std::string_view msg, Args&&... args)
{
    const auto message = format(msg, std::forward<Args>(args)...);
    push_prefixed(info_prefix(), module, message);
}

template <typename... Args>
void log_error(std::string_view module, std::string_view msg, Args&&... args)
{
    const auto message = format(msg, std::forward<Args>(args)...);
    push_prefixed(error_prefix(), module, message);
}

template <typename... Args>
void log_warning(std::string_view module, std::string_view msg, Args&&... args)
{
    const auto message = format(msg, std::forward<Args>(args)...);
    push_prefixed(warning_prefix(), module, message);
}

#define LOG_INFO(MODULE, X, ...) sofomo::log_info(MODULE, X __VA_OPT__(,) __VA_ARGS__)
#define LOG_ERROR(MODULE, X, ...) sofomo::log_error(MODULE, X  __VA_OPT__(,) __VA_ARGS__)
#define LOG_WARNING(MODULE, X, ...) sofomo::log_warning(MODULE, X __VA_OPT__(,) __VA_ARGS__)
}
