#pragma once

#include <string>
#include <string_view>
#include <filesystem>

#include "tools/filesystem.hpp"
#include "geolocation/geolocation.hpp"

constexpr std::string_view delimeter()
{
    return "{}";
}

template <typename T>
std::string toString(T arg)
{
    // Please, specialize this template for own classes
    return std::to_string(arg);
}

template <>
std::string toString(std::string msg);

template <>
std::string toString(const char* arg);

template <>
std::string toString(std::filesystem::path arg);

template <>
std::string toString(sofomo::tools::File file);

template <>
std::string toString(sofomo::Geolocation location);

template <typename T>
std::string construct(std::string_view msg, T&& arg)
{
    const auto arg_str = toString(arg);
    if (const auto found = msg.find(delimeter()); found != std::string::npos)
    {
        return std::string{std::begin(msg), std::begin(msg) + found} + arg_str + std::string{std::begin(msg) + found + 2, std::end(msg)};
    }
    return std::string{std::begin(msg), std::end(msg)};
}

template <typename T>
std::string format(std::string_view msg, T&& arg)
{
    return construct(msg, arg);
}

template <typename T, typename... Args>
std::string format(std::string_view msg, T&& arg, Args&&... args)
{
    const auto _msg = construct(msg, arg);
    return format(_msg, args...);
}
