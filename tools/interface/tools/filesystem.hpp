#pragma once

#include <string>

namespace sofomo
{

namespace tools
{

struct File
{
    File(const std::string& path);
    bool exists() const;
    bool create() const;
    bool remove() const;
  //  bool write(std::string_view content) const;
    std::string path() const;
private:
    std::string path_;
};

}

}