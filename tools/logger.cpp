#include "tools/logger.hpp"

#include <filesystem>
#include <mutex>

namespace sofomo
{
    std::mutex mutex;

    std::map<const char*, Logger::unique_ptr> loggers;

    void register_logger(const char* name, Logger::unique_ptr&& logger)
    {
        {
            std::unique_lock<std::mutex> lck{mutex};
            loggers.insert(std::make_pair(name, std::move(logger)));
        }
        LOG_INFO(MODULE_NAME, "Registered logger: {}", name);
    }

    std::size_t loggers_count()
    {
        return loggers.size();
    }

    void deregister_logger(const char* name)
    {
        {
            std::unique_lock<std::mutex> lck{mutex};
            const auto found = std::find_if(std::begin(loggers), std::end(loggers),
                                [&](const auto& pair) { return pair.first == name; });
            if (found == std::end(loggers)) return;
            loggers.erase(found);
        }
        LOG_INFO(MODULE_NAME, "Deregistered logger: {}", name);
    }

    void push_prefixed(std::string_view prefix, std::string_view module, std::string_view msg)
    {
        for (const auto& pair: loggers)
        {
            const auto& logger_ptr = pair.second;
            if (logger_ptr)
            {
                logger_ptr->destination() << '[' << prefix << "] <" << module << "> " << msg << std::endl;
            }
        }
    }

    void log_info(std::string_view module, std::string_view msg)
    {
        push_prefixed(info_prefix(), module, msg);
    }

    void log_error(std::string_view module, std::string_view msg)
    {
        push_prefixed(error_prefix(), module, msg);
    }

    void log_warning(std::string_view module, std::string_view msg)
    {
        push_prefixed(warning_prefix(), module, msg);
    }
}
