#include "tools/format.hpp"

#include <filesystem>

template <>
std::string toString(const char* arg)
{
    return arg;
}

template <>
std::string toString(std::string msg)
{
    return msg;
}

template <>
std::string toString(std::filesystem::path path)
{
    return path.string();
}

template <>
std::string toString(sofomo::tools::File file)
{
    return file.path();
}

template <>
std::string toString(sofomo::Geolocation location)
{
    return format("location[ip: {}, lon: {}, lat: {}]", location.ip, location.longitude, location.latitude);
}
