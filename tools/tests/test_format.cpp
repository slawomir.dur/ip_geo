#include "gtest/gtest.h"
#include "tools/format.hpp"

TEST(format, format)
{
    ASSERT_EQ(format("{}", "whatever"), "whatever");
    ASSERT_EQ(format("", 1), "");
    ASSERT_EQ(format("{}", 1), "1");
    ASSERT_EQ(format("   {}", 1), "   1");
    ASSERT_EQ(format("{}   ", 1), "1   ");
    ASSERT_EQ(format("{}{}", 1, 2, 3), "12");
    ASSERT_EQ(format("{}{}", 1), "1{}");
}