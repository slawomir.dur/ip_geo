cmake_minimum_required(VERSION 3.5)

project(SofomoTask VERSION 0.1 LANGUAGES CXX)

include(tests.cmake)
include(settings.cmake)
include(qt.cmake)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets Sql Network LinguistTools)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets Sql Network LinguistTools)

add_subdirectory(googletest)
add_subdirectory(tools)
add_subdirectory(geolocation)
add_subdirectory(database)
add_subdirectory(network)
add_subdirectory(ui)
add_subdirectory(app)

if(${QT_VERSION} VERSION_LESS 6.1.0)
    set(BUNDLE_ID_OPTION MACOSX_BUNDLE_GUI_IDENTIFIER com.example.SofomoTask)
endif()

set_target_properties(${PROJECT_NAME} PROPERTIES
    ${BUNDLE_ID_OPTION}
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE FALSE
)

include(GNUInstallDirs)
install(TARGETS ${PROJECT_NAME}
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
