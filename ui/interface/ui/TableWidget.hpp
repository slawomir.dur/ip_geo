#pragma once

#include <QTableView>

namespace sofomo
{

class GeolocationTableModel;

class TableWidget : public QTableView
{
    Q_OBJECT

public:
    TableWidget(QWidget* parent = nullptr);
    virtual ~TableWidget() = default;
    GeolocationTableModel* model();
private:
    void openPersistentEditors();
private slots:
    void onDataChanged();
private:
    GeolocationTableModel* model_;
};


}