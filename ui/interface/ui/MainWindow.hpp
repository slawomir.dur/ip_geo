#pragma once

#include <QMainWindow>
#include "geolocation/geolocation.hpp"

class QNetworkReply;
class QPushButton;

namespace sofomo
{

class NetGeolocationProvider;
class TableWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() = default;
private:
    void setLayout();
    void connect();
private slots:
    void addLocation(Geolocation location);
private:
    NetGeolocationProvider *locationProvider;
    TableWidget* table;
    QPushButton* checkMyIpButton;
};

}
