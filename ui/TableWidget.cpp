#include "ui/TableWidget.hpp"
#include "database/database.hpp"

#include "tools/logger.hpp"

namespace sofomo
{

TableWidget::TableWidget(QWidget* parent) : QTableView(parent)
{
    model_ = new GeolocationTableModel(this);
    this->setModel(model_);
    this->setItemDelegateForColumn(delete_button_index(), new GeolocationDelegate(this));
    this->openPersistentEditors();
    LOG_INFO(MODULE_NAME, "TableWidget created and connected to the model");

    QObject::connect(model_, &GeolocationTableModel::locationsChanged, this, &TableWidget::onDataChanged);
}

GeolocationTableModel* TableWidget::model()
{
    return model_;
}

void TableWidget::openPersistentEditors()
{
    const auto col_count = model_->columnCount();
    for (int row_id = 0; row_id < model_->rowCount(); ++row_id)
    {
        const auto index = model_->index(row_id, col_count - 1);
        this->openPersistentEditor(index);
    }
}

void TableWidget::onDataChanged()
{
    this->openPersistentEditors();
}

}