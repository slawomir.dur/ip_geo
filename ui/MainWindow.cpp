#include "ui/MainWindow.hpp"
#include "ui/TableWidget.hpp"

#include "network/network.hpp"
#include "database/database.hpp"

#include "tools/logger.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableView>
#include <QPushButton>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QList>
#include <QNetworkInterface>

/*
    Please, be mindful of the fact that Qt manages all raw pointers to Q_OBJECT
    marked classes itself, ensuring they deletion at the exit of the program
    at the latest. Hence, 'new' is not an issue on all QObjects.
*/
namespace sofomo
{

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), locationProvider{new NetGeolocationProvider{this}}
{
    setLayout();
    connect();
}

void MainWindow::setLayout()
{
    QWidget* widget = new QWidget{this};
    QVBoxLayout* vbox = new QVBoxLayout{this};
    
    table = new TableWidget{this};
    vbox->addWidget(table);

    checkMyIpButton = new QPushButton{tr("Check my IP"), this};
    vbox->addWidget(checkMyIpButton);

    widget->setLayout(vbox);
    setCentralWidget(widget);
    setGeometry(100, 100, 800, 600);
}

void MainWindow::connect()
{
    QObject::connect(checkMyIpButton, &QPushButton::clicked, [&]{ locationProvider->checkIpGeolocation(); });
    QObject::connect(locationProvider, &NetGeolocationProvider::geolocationReady,
        this, &MainWindow::addLocation);
}

void MainWindow::addLocation(Geolocation location)
{
    GeolocationTableModel* model = table->model();
    model->addLocation(location);
}

}
