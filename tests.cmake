enable_testing()

macro(add_test_executable TEST_NAME TEST_SOURCES)
	add_executable(${TEST_NAME} ${TEST_SOURCES})
	target_link_libraries(${TEST_NAME} PRIVATE gtest gtest_main)
	set_target_properties(${TEST_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin/tests")
	add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})
endmacro()