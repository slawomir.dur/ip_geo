#include "database/database.hpp"
#include "tools/format.hpp"
#include "tools/logger.hpp"
#include "tools/filesystem.hpp"
#include "geolocation/geolocation.hpp"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QModelIndex>

#include <QPushButton>

#include <filesystem>
#include <fstream>
#include <thread>

namespace sofomo
{

constexpr const char* db_file()
{
    return "db.sqlite";
}

struct DatabaseProvider
{
    static QSqlDatabase get_connection(const QString& connection_name)
    {
        QSqlDatabase db {QSqlDatabase::addDatabase("QSQLITE", connection_name)};
        db.setDatabaseName(db_file());
        if (!db.open())
        {
            LOG_ERROR(MODULE_NAME, "Error opening database: {}", db.lastError().text().toStdString());
            throw std::runtime_error("Error opening database");
        }
        const auto queryString = format("CREATE TABLE IF NOT EXISTS {} ("
                                        "ID INTEGER PRIMARY KEY,"
                                        "{} {},"
                                        "{} {},"
                                        "{} {})",
                                        GeolocationTableModel::table_name,
                                        GeolocationTableModel::ip_field.first, GeolocationTableModel::ip_field.second,
                                        GeolocationTableModel::latitude_field.first, GeolocationTableModel::latitude_field.second,
                                        GeolocationTableModel::longitude_field.first, GeolocationTableModel::longitude_field.second);
        QSqlQuery createTableQuery{db};
        if (!createTableQuery.exec(QString(queryString.c_str())))
        {
            LOG_ERROR(MODULE_NAME, "Could not create table: {}", createTableQuery.lastError().text().toStdString());
            throw std::runtime_error("Could not create table");
        }

        return db;
    }
};

DeleteButton::DeleteButton(const QModelIndex& index, const QString& label, QWidget* parent)
: QPushButton(label, parent), index{index} 
{
    QObject::connect(this, QPushButton::clicked, [&]{
        emit clickedDelete(this->index);
    });
}

GeolocationTableModel::GeolocationTableModel(QObject* parent)
 : QSqlTableModel(parent, DatabaseProvider::get_connection("GeolocationTableModel"))
{
    this->setTable(table_name);
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
    this->select();

    LOG_INFO(MODULE_NAME, "Table model set and connected OK");
}

void GeolocationTableModel::addLocation(Geolocation location)
{    
    QSqlRecord rec = record();

    rec.setValue(ip_field.first, location.ip.c_str());
    rec.setValue(latitude_field.first, location.latitude);
    rec.setValue(longitude_field.first, location.longitude);

    if (!insertRecord(-1, rec))
    {
        LOG_ERROR(MODULE_NAME, "Could not set new record");
        return;
    }
    if (!submitAll())
    {
        LOG_ERROR(MODULE_NAME, "Could not submit new record");
        return;
    }
    LOG_INFO(MODULE_NAME, "Geolocation submitted to db OK");
    emit locationsChanged();
}

int GeolocationTableModel::columnCount(const QModelIndex& parent) const
{
    return QSqlTableModel::columnCount(parent) + 1;
}

QVariant GeolocationTableModel::data(const QModelIndex& index, int role) const
{
    if (role == Qt::DisplayRole && index.column() == delete_button_index())
        return "Delete";
    return QSqlTableModel::data(index, role);
}

void GeolocationTableModel::deleteLocation(const QModelIndex& index)
{
    if (!removeRow(index.row()))
    {
        LOG_ERROR(MODULE_NAME, "Removing row {} failed", index.row());
        return;
    }

    if (!submitAll())
    {
        LOG_ERROR(MODULE_NAME, "Submitting row removal failed");
    }
    LOG_INFO(MODULE_NAME, "Removing row {} OK", index.row());
    emit locationsChanged();
}

GeolocationDelegate::GeolocationDelegate(QObject* parent) : QItemDelegate(parent) {}

QWidget* GeolocationDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() == delete_button_index())
    {
        DeleteButton* button = new DeleteButton{index, tr("Delete"), parent};

        const auto model = qobject_cast<const GeolocationTableModel*>(index.model());
        QObject::connect(button, &DeleteButton::clickedDelete, model, &GeolocationTableModel::deleteLocation);
        return button;
    }
    return QItemDelegate::createEditor(parent, option, index);
}

void GeolocationDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    if (index.column() == delete_button_index())
    {
        QPushButton* button = qobject_cast<QPushButton*>(editor);
        if (button)
        {
            button->setText(index.data().toString());
        }
    }
    return QItemDelegate::setEditorData(editor, index);
}

void GeolocationDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    return QItemDelegate::setModelData(editor, model, index);
}

void GeolocationDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    editor->setGeometry(option.rect);
}

}