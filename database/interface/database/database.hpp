#pragma once

#include <optional>

#include <QSqlTableModel>
#include <QItemDelegate>
#include <QPushButton>

class QSqlDatabase;
class QModelIndex;

namespace sofomo
{

class Geolocation;

constexpr int delete_button_index() { return 4; }

class GeolocationTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    GeolocationTableModel(QObject* parent = nullptr);
    void addLocation(Geolocation location);

    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
signals:
    void locationsChanged();

public slots:
    void deleteLocation(const QModelIndex& index);
public:

    static constexpr const char* table_name = "GEOLOCATION";
    static constexpr std::pair<const char*, const char*> ip_field =        std::make_pair("IP", "TEXT");
    static constexpr std::pair<const char*, const char*> latitude_field =  std::make_pair("LATITUDE", "REAL");
    static constexpr std::pair<const char*, const char*> longitude_field = std::make_pair("LONGITUDE", "REAL");
    static constexpr int new_record = -1;
};


class DeleteButton : public QPushButton
{
    Q_OBJECT
public:
    DeleteButton(const QModelIndex& index, const QString& label, QWidget* parent = nullptr);
signals:
    void clickedDelete(const QModelIndex& index);
private:
    const QModelIndex index;
};

class GeolocationDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    GeolocationDelegate(QObject* parent = nullptr);
    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const override;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override;
    virtual void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

}