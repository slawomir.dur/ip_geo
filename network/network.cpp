#include "network/network.hpp"
#include "tools/logger.hpp"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>

namespace sofomo
{

NetGeolocationProvider::NetGeolocationProvider(QObject* parent)
 : QObject{parent}, manager{new QNetworkAccessManager(this)}
{   
    QObject::connect(manager, &QNetworkAccessManager::finished,
                     this, &NetGeolocationProvider::replyFinished);
}

void NetGeolocationProvider::checkIpGeolocation(const QString& ipAddress)
{
    sendRequest(geolocUrl + ipAddress);
}

void NetGeolocationProvider::checkIpGeolocation()
{
    sendRequest(ownIpUrl);
}

void NetGeolocationProvider::replyFinished(QNetworkReply* reply)
{
    LOG_INFO(MODULE_NAME, "Response obtained from {}", reply->url().toString().toStdString());
    
    const auto deleter = [](QNetworkReply* reply) {reply->deleteLater();};
    std::unique_ptr<QNetworkReply, decltype(deleter)> guard(reply, deleter);

    if (reply->error() == QNetworkReply::NoError)
    {
        if (reply->url() == ownIpUrl) processOwnIp(reply);
        else if (reply->url().toString().contains(geolocUrl)) processGeolocation(reply);
    }
    else
    {
        LOG_ERROR(MODULE_NAME, "{}", reply->errorString().toStdString());
    }
}

void NetGeolocationProvider::sendRequest(const QString& urlString)
{
    manager->get(QNetworkRequest(QUrl(urlString)));
    LOG_INFO(MODULE_NAME, "Sent request: {}", urlString.toStdString());
}

void NetGeolocationProvider::processOwnIp(QNetworkReply* reply)
{
    checkIpGeolocation(reply->readAll());
}

void NetGeolocationProvider::processGeolocation(QNetworkReply* reply)
{
    QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
    if (json.isNull())
    {
        LOG_ERROR(MODULE_NAME, "Failed to parse json object");
        emit failed("Failed to parse json object");
        return;
    }

    QJsonObject object = json.object();

    const auto url = reply->url().toString();
    const auto ip = url.split('/').last();
    const auto longitude = object["lon"].toDouble();
    const auto latitude = object["lat"].toDouble();

    Geolocation location {ip.toStdString(), longitude, latitude};

    LOG_INFO(MODULE_NAME, "Geolocation object ready");
    emit geolocationReady(location);
}

}