#pragma once

#include <QObject>

#include "geolocation/geolocation.hpp"

class QNetworkReply;
class QString;
class QNetworkAccessManager;

class Geolocation;

namespace sofomo
{

class NetGeolocationProvider : public QObject
{
    Q_OBJECT
public:
    NetGeolocationProvider(QObject* parent = nullptr);
    void checkIpGeolocation(const QString& ipaddress);
    void checkIpGeolocation();
signals:
    void geolocationReady(Geolocation geolocation);
    void failed(const QString& msg);
private slots:
    void replyFinished(QNetworkReply* reply);
private:
    void sendRequest(const QString& urlString);
    void processOwnIp(QNetworkReply* reply);
    void processGeolocation(QNetworkReply* reply);
private:
    QNetworkAccessManager* manager;

    inline static const QString ownIpUrl = "https://ipinfo.io/ip";
    inline static const QString geolocUrl = "http://ip-api.com/json/";
};

}