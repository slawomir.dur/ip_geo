function(add_qt_interface LIB_NAME)
    message("Creating Qt interface: ${LIB_NAME}")
    set(LIB_SOURCES "${ARGN}")

    add_library(${LIB_NAME} INTERFACE)
    target_sources(${LIB_NAME} INTERFACE ${LIB_SOURCES})
    target_include_directories(${LIB_NAME} INTERFACE ${CMAKE_CURRENT_SOURCE_DIR} ${Qt6Widgets_INCLUDE_DIRS})

    set_property(GLOBAL APPEND PROPERTY GLOBAL_QT_INTERFACES ${LIB_NAME})
endfunction()

function(target_link_qt_interfaces TARGET)
    get_property(QT_INTERFACES GLOBAL PROPERTY GLOBAL_QT_INTERFACES)

    target_link_libraries(${TARGET} PRIVATE ${QT_INTERFACES})
endfunction()