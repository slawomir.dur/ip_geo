#include "ui/MainWindow.hpp"
#include "tools/logger.hpp"

#include <QApplication>
#include <QLocale>
#include <QTranslator>

#include <memory>

int main(int argc, char *argv[])
{
    auto logger = std::make_unique<sofomo::ConsoleLogger>();
    sofomo::register_logger("console", std::move(logger));

    QApplication a(argc, argv);
    LOG_INFO(MODULE_NAME, "Initialized application OK");

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "SofomoTask_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }
    LOG_INFO(MODULE_NAME, "Loaded translation files OK");

    sofomo::MainWindow w{};
    w.show();
    LOG_INFO(MODULE_NAME, "Initialized UI OK");
    
    return a.exec();
}
